
import pickle
from collections import Counter
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression as LR
from sklearn.model_selection import StratifiedKFold as KF
from sklearn.metrics import confusion_matrix as CM
from sklearn.neural_network import MLPClassifier as MLP
from sklearn.model_selection import GridSearchCV as gs
from imblearn.over_sampling import BorderlineSMOTE as SMOTE
from imblearn.over_sampling import ADASYN
from sklearn.preprocessing import StandardScaler, Normalizer, MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.ensemble import BaggingClassifier as BC
from sklearn.ensemble import AdaBoostClassifier as ADA
from sklearn.ensemble import GradientBoostingClassifier as GB
from sklearn.ensemble import VotingClassifier as VC
from sklearn.preprocessing import LabelEncoder
from scipy.stats import iqr
import sklearn.metrics as metrics
from sklearn.feature_selection import SelectKBest, chi2, f_classif, VarianceThreshold
from sklearn.tree import DecisionTreeClassifier as DT
import seaborn as sns
import matplotlib.pyplot as plt

## Function for training and validating models
def train_test(classifier_name, classifier, supersampler, gs_params, n_folds, classifier_to_file):
      kf = KF(n_splits = n_folds)
      i = 0
      # parameters for grid search
      
      for fold in kf.split(x, y):
            i+=1
            sm = supersampler
            cl = classifier

            # training fold samples
            x_fold = x[fold[0]]
            y_fold = y[fold[0]]

            # Supersampling
            x_res, y_res = sm.fit_resample(x_fold, y_fold)
            print(Counter(y_res))

            # Grid Search
            gs_ = gs(cl, gs_params, cv = 3, refit=True)
            gs_.fit(x_res, y_res)
            print(gs_.best_params_)

            # save classifier to disk to save time if reuse needed
            if classifier_to_file == 'Y':
                  with open(classifier_name+'_fold'+str(i)+'.pickle', 'wb') as handle:
                        pickle.dump(gs_, handle, protocol=pickle.HIGHEST_PROTOCOL) 
            
            # testing fold samples
            x_fold_test = x[fold[1]]
            y_fold_test = y[fold[1]]
            
            # results for testing samples
            y_fold_predicted = gs_.predict(x_fold_test)
            y_fold_predicted_proba = gs_.predict_proba(x_fold_test)
            
            # computing auc roc and confusion matrix
            fpr, tpr, threshold = metrics.roc_curve(y_fold_test, y_fold_predicted_proba[:, 1])
            roc_auc = metrics.auc(fpr, tpr)
            print(CM(y_fold_test, y_fold_predicted))     
            print('roc_auc =', roc_auc)


# Reading Dataset
dataset = pd.read_csv("./Data/orange_small_train.data", delimiter = '	')
labels_upselling = pd.read_csv("./Data/orange_small_train_upselling.labels", header=None)


# Concatenating labels to samples
dataset['label_upselling'] = labels_upselling
print(dataset.groupby('label_upselling').label_upselling.count())


# Drop NaN
#dataset = dataset.dropna(axis = 1, thresh = 25000) 
dataset = dataset.dropna(axis = 1, how = 'all')
#dataset = dataset.fillna(value = 0)
print("Dropped All NaN Features")
print(dataset.shape)


# Replace NaN
# Numeric NaN replaced by Mean
dataset.loc[:, :'Var190'] = dataset.loc[:, :'Var190'].apply(lambda x: x.fillna(x.mean()), axis = 0)
# Nominal NaN replaced by new class NaN_str
dataset = dataset.fillna('NaN_str')
print("Replaced Dataset NaN")


# Random Under Sampling of majority Class
# Concept discarted because of great class imbalance
#a_index = dataset[dataset['label_upselling']==-1].index
#b_index = dataset[dataset['label_upselling']==1].index.values
#sub_index = np.random.choice(a_index, 10000, replace=False)
#i = np.concatenate([b_index , sub_index])
#dataset = dataset.loc[i]


# Isolating labels from data
# Concatenation was used for undersampling
labels_upselling = dataset['label_upselling']
dataset = dataset.drop(['label_upselling'], axis = 1)

print(dataset.shape) 


# PCA
# PCA of numerical features isolated from categorical features
# PCA did not achieve better performance
#dataset_categorical = dataset[dataset.columns[dataset.apply(lambda col: int(col.name[3:]) > 190, axis = 0)]]
#dataset_numeric = dataset[dataset.columns[dataset.apply(lambda col: int(col.name[3:]) <= 190, axis = 0)]]
#dataset_numeric = StandardScaler().fit_transform(dataset_numeric)
#dataset_numeric = pd.DataFrame(PCA(n_components = 0.9).fit_transform(dataset_numeric), index = dataset.index)
#dataset = pd.concat([dataset_numeric, dataset_categorical], axis = 1)
#print(dataset.shape)


# Transforming nominal classes into numeric IDs
print("Dataset Label Encoding")
le = LabelEncoder()
dataset = dataset.apply(le.fit_transform) # automatically ignores numerical features


# Feature filtering by correlation threshold
print("Filtering Dataset by Correlation Threshold")
corr = dataset.corr().abs()
m = ~(corr.mask(np.eye(len(corr), dtype=bool)).abs() > 0.85).any() # threshold mask for correlation
#corr.loc[m,m].to_csv("corr_filtered.csv", sep = ',') # export correlation to csv file
dataset = dataset.loc[:, m]
print(dataset.shape)


# plot correlation heatmap
#p = sns.heatmap(corr)
#plt.show()        


# Discretization of categorical features
print("Dataset Binning of Categorical Features")

for c in dataset.columns.values: # Select first categorical feature remaining after above filters
   if int(c[3:]) > 190:
      first_cat = c
      break
dataset.loc[:, first_cat:] = dataset.loc[:, first_cat:].apply(lambda x: pd.cut(x, 7, labels = False), axis = 0) # create 7 bins per feature
#dataset.loc[:, first_cat:] = dataset.loc[:, first_cat:].apply(lambda x: pd.qcut(x, [0, .5, .75, 1], labels = False), axis = 0)

# Normalization of numerical features
#dataset.loc[:, :'Var190'] = MinMaxScaler().fit_transform(dataset.loc[:, :'Var190'])
#dataset.loc[:, :'Var191'] = Normalizer('l2').fit_transform(dataset.loc[:, :'Var191'])


# Feature filtering by score to labels with SelectKBest
print("Dataset K Best Filtered")

cols = dataset.columns # save dataset columns to identify categorical columns after filtering
skb = SelectKBest(chi2, k = 70)
dataset = skb.fit_transform(dataset, labels_upselling)
n_cat = sum(int(col[3:]) > 190 for col in cols[skb.get_support(indices= True)]) # number of categorical features remaining
cat_feats_index = list(range(dataset.shape[1]-n_cat, dataset.shape[1])) # Index of categorical features in filtered dataset
print(dataset.shape)



#### TRAININING AND RESULTS ####
 
x = dataset # data
y = labels_upselling.values.flatten() # classes

print('#################      GRADIENT BOOST         ##################')
param_grid_gb = {'n_estimators': [10, 50, 100, 200, 1000],
                  'learning_rate': [0.01, 0.1, 1]}                  
train_test('gradient_boost', GB(), SMOTE(), param_grid_gb, 5, 'N')

print('#################      BC         ##################')
param_grid_bc = {'n_estimators': [5, 10, 50, 100, 200]}
train_test('bagging_classifier', BC(), SMOTE(), param_grid_bc, 5, 'N')

print('#################      ADABOOST         ##################')
param_grid_ada = {'n_estimators': [10, 50, 100, 200], 
                  'learning_rate': [0.01, 0.1, 1, 10]}
train_test('ada_boost', ADA(), SMOTE(), param_grid_ada, 5, 'N')

print('#################      RF         ##################')
param_grid_rf = {'n_estimators': [5, 10, 50, 100, 200]}
train_test('random_forest', RF(), SMOTE(), param_grid_rf, 5, 'N')